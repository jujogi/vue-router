import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './Pages/Home.vue'
import Projects from './Pages/Projects.vue'
import Project from './Pages/Project.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: Home },
  { path: '/projects', component: Projects },
  { path: '/projects/:id', component: Project, props: true }
]

export default new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
})
